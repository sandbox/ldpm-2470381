<?php
/**
 * @file
 * This file contains Interfaces and Classes.
 */

/**
 * Interface FormatterInterface.
 *
 * Formatters are responsible for creating machine-readable output.
 */
interface FormatterInterface {

  /**
   * Formatter class must implement generateOutput().
   */
  public function generateOutput();

  /**
   * Formatter class must implement setMonitor().
   *
   * @param \MonitorInterface $monitor
   *    Dependency injection.
   */
  public function setMonitor(MonitorInterface $monitor);

}


/**
 * Interface MonitorInterface.
 */
interface MonitorInterface {
  /**
   * Monitor Class must implement checkUser1().
   */
  public function checkUser1();

  /**
   * Monitor Class must implement checkCron().
   */
  public function checkCron();

  /**
   * Monitor Class must implement checkDevel().
   */
  public function checkDevel();

}

/**
 * Class StandardMonitor.
 */
class StandardMonitor implements MonitorInterface {
  public $cronLast;
  public $cronInterval;


  /**
   * Implements constructor.
   */
  public function __construct() {
    $this->cronLast = variable_get('cron_last');
    $this->cronInterval = (int) variable_get('op_manager_status_cron_interval', LIGHTWEIGHT_HEALTH_MONITOR_DEFAULT_CRON_INTERVAL);
  }

  /**
   * Function checkCron().
   *
   * Return the number of seconds in which cron is due. If this is negative,
   * there is no problem. If this is positive, cron is overdue. The formatter
   * will be in charge of deciding what kind of alert this should give.
   */
  public function checkCron() {
    $now = time();
    $due = strtotime("+$this->cronInterval minutes", $this->cronLast);
    return $now - $due;
  }

  /**
   * Function checkUser1().
   *
   * Check whether User 1 is currently Active. Keeping User 1 blocked unless
   * there is a specific task you need (such as to run update.php) is good
   * practice.
   *
   * @return bool
   *   (TRUE if ACTIVE).
   */
  public function checkUser1() {
    $user1 = user_load('1');
    return $user1->status;
  }

  /**
   * Function checkDevel().
   *
   * Leaving the Devel module on in production is a bad idea, but easy to do.
   *
   * @return bool
   *    (TRUE if the module is on)
   */
  public function checkDevel() {
    return module_exists('devel');
  }

}
