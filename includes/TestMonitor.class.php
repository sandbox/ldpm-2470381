<?php
/**
 * @file
 * This file contains a test Monitor that will always return OK for everything.
 */

/**
 * Class TestMonitorFail. All tests should fail.
 */
class TestMonitorFail implements MonitorInterface {
  /**
   * Function checkCron().
   *
   * Will always return overdue by 100 minutes.
   */
  public function checkCron() {
    return 100;
  }

  /**
   * Function checkUser1().
   *
   * @return bool
   *   (Test class will always return TRUE)
   */
  public function checkUser1() {
    return TRUE;
  }

  /**
   * Function checkDevel().
   *
   * @return bool
   *    (Test class will always return TRUE)
   */
  public function checkDevel() {
    return TRUE;
  }

}

/**
 * Class TestMonitorSucceed. All tests should succeed.
 */
class TestMonitorSucceed implements MonitorInterface {
  /**
   * Function checkCron().
   *
   * Will always return overdue by 100 minutes.
   */
  public function checkCron() {
    return -100;
  }

  /**
   * Function checkUser1().
   *
   * @return bool
   *   (Test class will always return TRUE)
   */
  public function checkUser1() {
    return FALSE;
  }

  /**
   * Function checkDevel().
   *
   * @return bool
   *    (Test class will always return TRUE)
   */
  public function checkDevel() {
    return FALSE;
  }

}
