<?php
/**
 * @file
 * Administrative menu include for Lightweight Health Monitor module.
 */

/**
 * Implements callback for the settings form.
 *
 * @return mixed
 *    Returns an array of form elements rendered by Drupal.
 */
function lightweight_health_monitor_settings_form() {
  $form = array();
  $form['lightweight_health_monitor_cron_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum Cron interval'),
    '#description' => t('What is the maximum allowable amount of time in minutes between cron runs?'),
    '#default_value' => variable_get('lightweight_health_monitor_cron_interval', LIGHTWEIGHT_HEALTH_MONITOR_DEFAULT_CRON_INTERVAL),
  );
  return system_settings_form($form);
}
