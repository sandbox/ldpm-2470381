<?php
/**
 * @file
 * PHPUnit tests for the Lightweight Health Monitor module.
 *
 * To run:
 * cd /path/to/drupal
 * phpunit lightweightHealthMonitorTests \
 * sites/[your/module/path]/tests/HumanTest.php.
 *
 * @todo how to fix DRUPAL_ROOT to work for everyone?
 */

// ----------- BEGIN BOOTSTRAP BLOCK ----------------
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
require_once './includes/bootstrap.inc';
define('DRUPAL_ROOT', '/var/www/leadership/drupal');
set_include_path(DRUPAL_ROOT . PATH_SEPARATOR . get_include_path());
set_include_path(get_include_path() . PATH_SEPARATOR .
  "/var/www/leadership/drupal/sites/all/modules/acp/lightweight_health_monitor/includes/");
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
// ----------- END BOOTSTRAP BLOCK ------------------

/**
 * Class lightweightHealthMonitorHumanTest.
 */
class LHMHumanTest extends PHPUnit_Framework_TestCase {

  /**
   * Tests should all fail.
   */
  public function testMonitorFail() {
    require_once("TestMonitor.class.php");
    $formatter = new HumanFormatter();
    $monitor = new TestMonitorFail();
    $formatter->setMonitor($monitor);
    $output = $formatter->generateOutput();
    // Make sure a table was rendered.
    $caption_matcher = array(
      'tag' => 'caption',
      'content' => 'Lightweight Health Monitor Results:',
      'ancestor' => array('tag' => 'table'),
    );
    $this->assertTag(
      $caption_matcher,
      $output,
      "failed to match caption",
      TRUE
    );
    // Make sure cron is in an error state.
    $cron_matcher = array(
      'id' => 'cron_data',
      'tag' => 'td',
      'attributes' => array('class' => 'error'),
      'content' => 'Cron has not run',
    );
    $this->assertTag(
      $cron_matcher,
      $output,
      "cron monitor did not fail as expected: $output",
      TRUE
    );
    // Make sure User 1 is in an error state.
    $user1_matcher = array(
      'id' => 'user1_data',
      'tag' => 'td',
      'attributes' => array('class' => 'error'),
      'content' => 'ACTIVE',
    );
    $this->assertTag(
      $user1_matcher,
      $output,
      "User 1 monitor did not fail as expected: $output",
      TRUE
    );
    // Make sure Devel is in an error state.
    $devel_matcher = array(
      'id' => 'devel_data',
      'tag' => 'td',
      'attributes' => array('class' => 'error'),
      'content' => 'ACTIVE',
    );
    $this->assertTag(
      $devel_matcher,
      $output,
      "devel monitor did not fail as expected: $output",
      TRUE
    );
  }

  /**
   * Tests should all succeed.
   */
  public function testMonitorSucceed() {
    $formatter = new HumanFormatter();
    $monitor = new TestMonitorSucceed();
    $formatter->setMonitor($monitor);
    $output = $formatter->generateOutput();
    // Make sure a table was rendered.
    $caption_matcher = array(
      'tag' => 'caption',
      'content' => 'Lightweight Health Monitor Results:',
      'ancestor' => array('tag' => 'table'),
    );
    $this->assertTag(
      $caption_matcher,
      $output,
      "failed to match caption",
      TRUE
    );
    // Make sure cron is in an error state.
    $cron_matcher = array(
      'id' => 'cron_data',
      'tag' => 'td',
      'attributes' => array('class' => 'ok'),
      'content' => 'OK',
    );
    $this->assertTag(
      $cron_matcher,
      $output,
      "cron monitor did not fail as expected: $output",
      TRUE
    );
    // Make sure User 1 is in an error state.
    $user1_matcher = array(
      'id' => 'user1_data',
      'tag' => 'td',
      'attributes' => array('class' => 'ok'),
      'content' => 'OK',
    );
    $this->assertTag(
      $user1_matcher,
      $output,
      "User 1 monitor did not fail as expected: $output",
      TRUE
    );
    // Make sure Devel is in an error state.
    $devel_matcher = array(
      'id' => 'devel_data',
      'tag' => 'td',
      'attributes' => array('class' => 'ok'),
      'content' => 'OK',
    );
    $this->assertTag(
      $devel_matcher,
      $output,
      "devel monitor did not fail as expected: $output",
      TRUE
    );
  }

}
