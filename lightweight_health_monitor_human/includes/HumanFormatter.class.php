<?php
/**
 * @file
 * Op Manager Formatter Class.
 */

/**
 * Class OpManagerFormatter.
 */
class HumanFormatter implements FormatterInterface {
  private $monitor;

  /**
   * Class function setMonitor().
   *
   * @param \MonitorInterface $monitor
   *    Monitor object is passed in as a setter injector.
   */
  public function setMonitor(MonitorInterface $monitor) {
    $this->monitor = $monitor;
  }

  /**
   * Class Function generateOutput().
   *
   * @return string
   *    Returns human-readable output.
   */
  public function generateOutput() {
    $header = array("Status", "Monitor", "Description");
    $rows = array();

    $cron_result = $this->monitor->checkCron();
    if ($cron_result < 0) {
      $cron_data = array(
        'data' => 'OK',
        'id' => 'cron_data',
        'class' => 'ok'
      );
    }
    else {
      $dtf = new DateTime("@0");
      $dtt = new DateTime("@$cron_result");
      $cron_human = $dtf->diff($dtt)->format('Cron has not run in %a days and %h hours');
      $cron_data = array(
        'data' => $cron_human,
        'id' => 'cron_data',
        'class' => 'error',
      );
    }

    $rows[] = array(
      $cron_data,
      "CRON",
      "Checks the last time CRON was run, compared to the maximum acceptable duration.",
    );
    if ($this->monitor->checkUser1()) {
      $user1_data = array(
        'data' => "ACTIVE",
        'id' => 'user1_data',
        "class" => "error",
      );
    }
    else {
      $user1_data = array(
        "data" => "OK",
        'id' => 'user1_data',
        "class" => "ok",
      );
    }
    $rows[] = array(
      $user1_data,
      "USER 1",
      "Checks to see if <a href='/user/1'>User 1</a> (the Drupal Superuser) is enabled on this site",
    );
    if ($this->monitor->checkDevel()) {
      $devel_data = array(
        'data' => "ACTIVE",
        'id' => 'devel_data',
        "class" => "error",
      );
    }
    else {
      $devel_data = array(
        "data" => "OK",
        'id' => 'devel_data',
        "class" => "ok",
      );
    }

    $rows[] = array(
      $devel_data,
      "DEVEL",
      "Checks to see if the <a href='/admin/modules/#development'>devel module</a> is enabled on this site",
    );
    $variables = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(),
      'caption' => 'Lightweight Health Monitor Results:',
      'colgroups' => array(),
      'sticky' => TRUE,
      'empty' => 'No Monitors Found',
    );
    return theme_table($variables);
  }

}
