<?php
/**
 * @file
 * Op Manager Formatter Class.
 */

/**
 * Class OpManagerFormatter.
 */
class OpManagerFormatter implements FormatterInterface {
  private $monitor;

  /**
   * Class function setMonitor().
   *
   * @param \MonitorInterface $monitor
   *    Monitor object is passed in as a setter injector.
   */
  public function setMonitor(MonitorInterface $monitor) {
    $this->monitor = $monitor;
  }

  /**
   * Class Function generateOutput().
   *
   * @return string
   *    Returns JSON-encoded statistics from each monitor function.
   */
  public function generateOutput() {
    $output = array();
    $cron_result = $this->monitor->checkCron();
    if ($cron_result > 0) {
      $output['CRON'] = "OK";
    }
    else {
      $output['CRON'] = "OVERDUE";
    }
    $output['USER 1'] = $this->monitor->checkUser1() ? "ACTIVE" : "OK";
    $output['DEVEL'] = $this->monitor->checkDevel() ? "ACTIVE" : "OK";
    return json_encode($output);
  }

}
