<?php
/**
 * @file
 * PHPUnit tests for the Lightweight Health Monitor module.
 *
 * To run:
 * cd /path/to/drupal
 * phpunit lightweightHealthMonitorTests \
 * sites/[your/module/path]/lightweightHealthMonitorTest.php.
 *
 * @todo how to fix DRUPAL_ROOT to work for everyone?
 */

// ----------- BEGIN BOOTSTRAP BLOCK ----------------
$_SERVER['HTTP_HOST'] = 'portal.afilias.info';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
require_once './includes/bootstrap.inc';
define('DRUPAL_ROOT', '/var/www/leadership/drupal');
set_include_path(DRUPAL_ROOT . PATH_SEPARATOR . get_include_path());
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
// ----------- END BOOTSTRAP BLOCK ------------------

class lightweightHealthMonitorTest extends PHPUnit_Framework_TestCase {
  /**
   * Test.
   */
  public function testTest() {
    $this->assertEquals("1", "1", "no tautology");
  }

}
