The Lightweight Health Monitor provides a few basic monitors to ensure your 
site is "production ready":

 * whether cron has run recently
 * whether User 1 is active
 * whether the devel module is enabled
  
It comes with two interfaces: a GUI and a simple JSON output. The JSON is
suitable for monitoring by Op Manager using Op Manager's Content Match feature.

 * If you are using Nagios, and you are certain that you will never 
 accidentally leave the devel module enabled in production, you might be better 
 off  with the very fine Nagios module here on d.o (http://drupal
 .org/projects/nagios).
 * If you require a lot more parameters to be monitored, or finer grain 
 control over what you monitor, you might be better off with the also very 
 fine Monitoring module here on d.o (http://drupal.org/projects/monitoring).
 * If, on the other hand, if you've ever accidentally logged in as User 1 on 
 production, your needs are modest, or you aren't yet sure what you need but 
 you know that just looking for an HTTP 200 response isn't good enough, 
 Lightweight Health Monitor could be for you.
