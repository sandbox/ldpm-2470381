Installation Instructions
=========================

Enable the modules
------------------
  1. Under the Modules page (/admin/modules), enable the "Lightweight Health 
  Monitor" module under the "Lightweight Health Monitor" package.
  2. Choose at least one Formatter module; the Lightweight Health Monitor 
  module alone won't actually monitor anything.
  3. Save changes.

Set permissions
---------------
Each 

Confirm success by visiting the monitor status pages:
-----------------------------------------------------
The Op Manager / JSON output will be available at 
http://your.site/monitors/lhm/json.

The Human Readable output will be available at
http://your.site/monitors/lhm/human.